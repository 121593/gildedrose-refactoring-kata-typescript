# GildedRose-Refactoring-Kata - typescript
[![](https://gitlab.com/121593/gildedrose-refactoring-kata-typescript/badges/master/pipeline.svg)](https://gitlab.com/metrocheck/internal/server/-/pipelines/latest)

# Install
- Clone this repository
- Run `npm install`

# Tests
- Run tests with : `npm run test`
- Get coverage with `npm run coverage`
