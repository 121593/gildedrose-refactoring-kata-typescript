import { expect } from 'chai';
import { GildedRose } from '../app/GildedRose';
import cases from './cases'

for(const {item, expectedQuality, expectedSellIn} of cases()) {
    describe(item.name , function() {
        it('Should have expected quality and sellIn after one update', function(done) {
            const gildedRoseInstance = new GildedRose([item])

            gildedRoseInstance.updateQuality()

            expect(gildedRoseInstance.items[0].quality).to.equal(expectedQuality)
            expect(gildedRoseInstance.items[0].sellIn).to.equal(expectedSellIn)

            done()
        });
    });
}
