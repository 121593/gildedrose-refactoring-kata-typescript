import Regular from "../app/model/Items/Regular";
import Ageable from "../app/model/Items/Ageable";
import BackstagePass from "../app/model/Items/BackstagePass";
import Conjured from "../app/model/Items/Conjured";
import Legendary from "../app/model/Items/Legendary";
import AbstractItem from "../app/model/AbstractItem";

export interface CaseInterface {
    item: AbstractItem
    expectedSellIn: number|null
    expectedQuality: number
}

export default (): Array<CaseInterface>  => [{
        item: new Regular("+5 Dexterity Vest", 10, 20), expectedSellIn: 9, expectedQuality: 19
    },{
        item: new Regular("Sold expired", -1, 10), expectedSellIn: -2, expectedQuality: 8
    },{
        item: new Regular("Not under 0", 5, 0), expectedSellIn: 4, expectedQuality: 0
    },{
        item: new Ageable("Aged Brie", 2, 0), expectedSellIn: 1, expectedQuality: 1
    },{
        item: new Legendary("Legendary item", 80), expectedSellIn: null, expectedQuality: 80
    },{
        item: new BackstagePass("Backstage passes at D-15", 15, 20), expectedSellIn: 14, expectedQuality: 21
    },{
        item: new BackstagePass("Backstage passes at D-10", 10, 47), expectedSellIn: 9, expectedQuality: 49
    }, {
        item: new BackstagePass("Backstage passes maxhit", 5, 49), expectedSellIn: 4, expectedQuality: 50
    }, {
        item: new BackstagePass("Backstage passes at D-5", 5, 20), expectedSellIn: 4, expectedQuality: 23
    },{
        item: new BackstagePass("Backstage passes expired", 0, 30), expectedSellIn: -1, expectedQuality: 0
    },{
        item: new Conjured("Conjured Mana Cake", 3, 6), expectedSellIn: 2, expectedQuality: 4
    },{
        item: new Conjured("Conjured and expired so lame", -1, 6), expectedSellIn: -2, expectedQuality: 2
    }
]
