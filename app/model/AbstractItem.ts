import Item from './Item'
export default abstract class AbstractItem extends Item{

    constructor(name: string, sellIn: number|null, quality: number) {
        super(name, sellIn, quality);
    }

    // eslint-disable-next-line @typescript-eslint/no-inferrable-types
    protected _qualityTreshold: number = -1

    protected get qualityTreshold() :number {
        // Once the sell by date has passed, Quality degrades twice as fast
        if(this.sellIn < 0) { return 2 * this._qualityTreshold }

        return this._qualityTreshold
    }

    public updateQuality(): void {
        this.tickSellIn()
        this.tickQuality()
    }

    protected tickSellIn(): void {
        this.sellIn--
    }

    protected tickQuality(): void {
        this.quality +=
            // The Quality of an item is never negative
            (this.quality + this.qualityTreshold < 0) ? -this.quality
            // The Quality of an item is never more than 50
            : (this.qualityTreshold && 50 - this.quality < this.qualityTreshold) ? 50 - this.quality
            : this.qualityTreshold
    }
}
