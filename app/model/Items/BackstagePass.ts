import AbstractItem from "../AbstractItem";

export default class BackstagePass extends AbstractItem{

    // Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less
    protected get qualityTreshold(): number {
        if(this.sellIn > 10) return 1
        if(this.sellIn <= 10 && this.sellIn > 5) return 2
        if(this.sellIn <= 5 && this.sellIn > 0) return 3
        return 0
    }

    protected tickQuality(): void {
        super.tickQuality();

        // Quality drops to 0 after the concert
        if(this.sellIn < 0) { this.quality = 0; }
    }
}
