import AbstractItem from "../AbstractItem";

export default class Legendary extends AbstractItem {
    // however "Sulfuras" is a legendary item and as such its Quality is 80 and it never alters.
    protected _qualityTreshold = 0

    constructor(name: string, quality: number) {
        super(name, null, quality);
    }

    // being a legendary item, never has to be sold
    protected tickSellIn(): void {
        return
    }

    protected tickQuality():void {
        return
    }
}
