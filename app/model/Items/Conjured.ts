import AbstractItem from "../AbstractItem";

export default class Conjured extends AbstractItem {
    //  "Conjured" items degrade in Quality twice as fast as normal items
    protected _qualityTreshold = -2
}
