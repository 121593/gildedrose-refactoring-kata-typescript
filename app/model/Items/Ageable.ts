import AbstractItem from "../AbstractItem";

export default class Ageable extends AbstractItem {
    // Aged Brie" actually increases in Quality the older it gets
    _qualityTreshold = 1

    // No double bonus
    protected get qualityTreshold() :number {
        return this._qualityTreshold
    }
}
