import AbstractItem from "./model/AbstractItem";

export class GildedRose {
    items: Array<AbstractItem>;

    constructor(items: Array<AbstractItem>) {
        this.items = items;
    }

    updateQuality(): void {
        this.items.forEach(item => item.updateQuality())
    }
}
